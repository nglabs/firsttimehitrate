﻿using System.Collections;
using UnityEngine;
using VRStandardAssets.Utils;

public class Objective : MonoBehaviour {

    [SerializeField]
    public string ObjectiveTitle;

    [SerializeField]
    public string ObjectiveText;

    [SerializeField]
    private TriggerType TriggerType;

    private ObjectiveManager _ObjectiveManager;

    private AudioSource _Audio;

    private bool m_GazeOver = false;

    private VRInput vri;

    VRInteractiveItem vii;

    void Start()
    {
        _ObjectiveManager = GameObject.Find("ObjectiveManager").GetComponent<ObjectiveManager>();
        _Audio = GetComponent<AudioSource>();

        if(TriggerType == TriggerType.Click && gameObject.GetComponent<VRInteractiveItem>())
        {
            vri = GameObject.FindGameObjectWithTag("Player").GetComponent<VRInput>();
            vri.OnClick += Vri_OnClick;

            vii = gameObject.GetComponent<VRInteractiveItem>();
            vii.OnOver += Vii_OnOver;
            vii.OnOut += Vii_OnOut;
        }
    }

    void OnDestroy()
    {
        if (TriggerType == TriggerType.Click && gameObject.GetComponent<VRInteractiveItem>())
        {
            vri.OnClick -= Vri_OnClick;
            vii.OnOver -= Vii_OnOver;
            vii.OnOut -= Vii_OnOut;
        }
    }

    private void Vii_OnOut()
    {
        m_GazeOver = false;
    }

    private void Vii_OnOver()
    {
        m_GazeOver = true;
    }

    private void Vri_OnClick()
    {
        if (this != _ObjectiveManager.Objectives[_ObjectiveManager.CurrentObjective])
        {
            return;
        }

        if (TriggerType == TriggerType.Click && m_GazeOver)
        {
            _ObjectiveManager.CurrentObjective++;
            toggleObject(false);
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (_Audio && !_Audio.isPlaying)
        {
            if (this != _ObjectiveManager.Objectives[_ObjectiveManager.CurrentObjective])
            {
                toggleObject(false);
            }
            else
            {
                toggleObject(true);
            }
        } 
        else
        {
            if (this != _ObjectiveManager.Objectives[_ObjectiveManager.CurrentObjective])
            {
                toggleObject(false);
            }
            else
            {
                toggleObject(true);
            }
        }
    }

    IEnumerator OnTriggerEnter (Collider col)
    {
        if (this != _ObjectiveManager.Objectives[_ObjectiveManager.CurrentObjective])
        {
            yield break;
        }

        if (TriggerType == TriggerType.Collision)
        {
            if (col.tag == "Player")
            {
                _ObjectiveManager.CurrentObjective++;
                toggleObject(false);

                if (_Audio)
                {
                    _Audio.Play();
                    yield return new WaitForSeconds(_Audio.clip.length);
                }
                
                Destroy(gameObject);
            }
        }
    }

    IEnumerator OnMouseDown()
    {
        if (this != _ObjectiveManager.Objectives[_ObjectiveManager.CurrentObjective])
        {
            yield break;
        }

        if (TriggerType == TriggerType.Click)
        {
            _ObjectiveManager.CurrentObjective++;
            toggleObject(false);

            if (_Audio)
            {
                _Audio.Play();
                yield return new WaitForSeconds(_Audio.clip.length);
            }

            Destroy(gameObject);
        }
    }

    void toggleObject(bool enable)
    {
        if (GetComponent<Collider>())
        {
            GetComponent<Collider>().enabled = enable;
        }
        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().enabled = enable;
        }
        if (GetComponent<BoxCollider>())
        {
            GetComponent<BoxCollider>().enabled = enable;
        }
        if (GetComponent<CapsuleCollider>())
        {
            GetComponent<CapsuleCollider>().enabled = enable;
        }
        if (GetComponent<Renderer>())
        {
            GetComponent<Renderer>().enabled = enable;
        }

        foreach (Renderer r in GetComponentsInChildren<Renderer>())
            r.enabled = enable;
    }
}

public enum TriggerType
{
    Click = 0,
    Collision = 1
}

