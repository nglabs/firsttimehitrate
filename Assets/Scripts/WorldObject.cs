﻿using UnityEngine;
using System.Collections;

public class WorldObject : MonoBehaviour {

    [SerializeField]
    private bool Appear;

    [SerializeField]
    private int ObjectiveToAppearOn;

    [SerializeField]
    private bool Disappear;

    [SerializeField]
    private int ObjectiveToDisappearOn;

    private ObjectiveManager _ObjectiveManager;

    void Start()
    {
        _ObjectiveManager = GameObject.Find("ObjectiveManager").GetComponent<ObjectiveManager>();

        if (Appear)
        {
            toggleObject(false);
        }

        if (Disappear && ObjectiveToAppearOn >= ObjectiveToDisappearOn)
        {
            Destroy(gameObject);
        }
    }

    void Update () {

        int currentObjective = _ObjectiveManager.CurrentObjective;
        
        if (Disappear && currentObjective >= ObjectiveToDisappearOn)
        {
            Destroy(gameObject);
        }
        
        if (Appear && currentObjective >= ObjectiveToAppearOn)
        {
            toggleObject(true);
        }
	}

    void toggleObject(bool enable)
    {
        if (GetComponent<Collider>())
        {
            GetComponent<Collider>().enabled = enable;
        }
        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().enabled = enable;
        }
        if (GetComponent<BoxCollider>())
        {
            GetComponent<BoxCollider>().enabled = enable;
        }
        if (GetComponent<CapsuleCollider>())
        {
            GetComponent<CapsuleCollider>().enabled = enable;
        }
        if (GetComponent<Renderer>())
        {
            GetComponent<Renderer>().enabled = enable;
        }
        
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
            r.enabled = enable;
    }

}

public enum WorldObjectEffect
{
    Appear = 0,
    Disappear = 1
}
