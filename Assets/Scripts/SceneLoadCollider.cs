﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoadCollider : MonoBehaviour
{

    [SerializeField]
    private string m_SceneToLoad = "";

    void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene(m_SceneToLoad);
    }
    

}
