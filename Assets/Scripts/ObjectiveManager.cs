﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectiveManager : MonoBehaviour {

    [SerializeField]
    public Objective[] Objectives;

    [SerializeField]
    private Text TitleOutput;

    [SerializeField]
    private Text TextOutput;

    [SerializeField]
    public int CurrentObjective = 0;
    
    void Start() {
        CurrentObjective = 0;
    }

    void Update ()
    {
        if (CurrentObjective < Objectives.Length)
        {
            TitleOutput.text = Objectives[CurrentObjective].ObjectiveTitle;
            TextOutput.text = Objectives[CurrentObjective].ObjectiveText;
        }
        else
        {
            TitleOutput.text = "Well done!";
            TextOutput.text = "You have completed all of the objectives.";
            // End game, all objectives have been completed
        }
    }
}
