# First Time Hit Rate #

A Virtual Reality (VR) training experience to enhance the existing First Time Hit Rate (FTHR) Standard Operating Procedure (SOP) training process, with the aim of improving trainee engagement.